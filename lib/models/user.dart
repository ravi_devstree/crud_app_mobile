import 'dart:math' as math;

class User {
  int id;
  String name;
  int count;

  static Map<String, String> randomUser() {
    List<String> names = ['Ravi', 'James', 'Surbhi', 'Johar'];
    return {'name': names[math.Random().nextInt(names.length)], 'count': '0'};
  }

  User.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.name = json['name'];
    this.count = json['count'];
  }

  Map<String, String> increasedCountJson() {
    return {'name': name, 'count': '${count + 1}'};
  }
}
