import 'package:flutter/material.dart';

import 'bloc/user_bloc.dart';
import 'models/user.dart';

class UserList extends StatelessWidget {
  final UserBloC userBloC = UserBloC();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CRUD Operation'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh, color: Colors.white),
            onPressed: userBloC.getUserList(),
          )
        ],
      ),
      body: StreamBuilder<List<User>>(
        stream: userBloC.outputUser,
        initialData: [],
        builder: (BuildContext context, AsyncSnapshot<List<User>> snapshot) {
          return ListView.separated(
            itemCount: snapshot.data.length,
            itemBuilder: (BuildContext context, int index) {
              User user = snapshot.data[index];
              return Dismissible(
                child: ListTile(
                  title: Text(user.name),
                  leading: Text(
                    '${user.count}',
                    style: Theme.of(context).textTheme.display1,
                  ),
                  subtitle: Text('Tap to update count'),
                  trailing: IconButton(
                    icon: Icon(
                      Icons.delete_forever,
                      color: Colors.red,
                    ),
                    onPressed: () {
                      userBloC.deleteUser(user);
                    },
                  ),
                  onTap: () {
                    userBloC.increaseCount(user);
                  },
                ),
                key: UniqueKey(),
                background: Container(color: Colors.red),
                onDismissed: ((direction) {
                  userBloC.deleteUser(user);
                }),
              );
            },
            separatorBuilder: (BuildContext context, int index) {
              return Divider();
            },
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          userBloC.addNewUser();
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
